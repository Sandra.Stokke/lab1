package rockPaperScissors;

import java.sql.SQLOutput;
import java.util.*;

import static java.lang.System.out;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    Random rand = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    String humanChose ="";
    String computerChose="";
    int numb = rand.nextInt(3);
    public void run() {
        // TODO: Implement Rock Paper Scissors
            while (true) {
                System.out.println("Let's play round " + roundCounter);

                // Human Chose
                while (true) {
                System.out.println("Your choice (Rock/Paper/Scissors)?");
                humanChose = sc.nextLine().toLowerCase();

                    if (humanChose.equals("rock") || humanChose.equals("paper") || humanChose.equals("scissors"))
                       break;

                    else System.out.println("I don't understand " + humanChose + ". Could you try again?");
                }
                // Computer Chose

                if (numb == 0) {
                    computerChose = "rock";
                } else if (numb == 1) {
                    computerChose = "paper";
                } else if (numb == 2) {
                    computerChose = "scissors";
                }

                // playing the game
                if (humanChose.equals(computerChose)) {
                    System.out.println("Human chose " + humanChose + ", computer chose " + computerChose + ". It's a tie!");
                } else if (humanChose.equals("rock") && computerChose.equals("paper")) {
                    computerScore++;
                    System.out.println(("Human chose " + humanChose + ", computer chose " + computerChose + ". Computer Wins!"));
                } else if (humanChose.equals("paper") && computerChose.equals("scissors")) {
                    computerScore++;
                    System.out.println(("Human chose " + humanChose + ", computer chose " + computerChose + ". Computer Wins!"));
                } else if (humanChose.equals("scissors") && computerChose.equals("rock")) {
                    computerScore++;
                    System.out.println(("Human chose " + humanChose + ", computer chose " + computerChose + ". Computer Wins!"));
                } else if (humanChose.equals("rock") && computerChose.equals("scissors")) {
                    humanScore++;
                    System.out.println(("Human chose " + humanChose + ", computer chose " + computerChose + ". Human Wins!"));
                } else if (humanChose.equals("paper") && computerChose.equals("rock")) {
                    humanScore++;
                    System.out.println("Human chose " + humanChose + ", computer chose " + computerChose + ". Human Wins!");
                } else if (humanChose.equals("scissors") && computerChose.equals("paper")) {
                    humanScore++;
                    System.out.println(("Human chose " + humanChose + ", computer chose " + computerChose + ". Human Wins!"));
                }
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                System.out.println("Do you wish to continue playing? (y/n)?");
                String answer = sc.nextLine();
                if (answer.equals("y")) {
                    roundCounter++;
                } else if (answer.equals("n")) {
                    System.out.println("Bye bye :)");
                    break;
                }
                }
            }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}